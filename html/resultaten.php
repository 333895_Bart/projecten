<?php
   include("./connect_db.php");

   $sql = "SELECT * FROM `scoreformulier`";

    // dit is de functie die de query $sql via de verbinding $conn naar de database stuurt
   $result = mysqli_query($conn, $sql);

   $records = "";
   while ($record = mysqli_fetch_assoc($result)){
     $records .= "<tr>
       <td>{$record['naam']}</td>
       <td>{$record['email']}</td>
       <td>{$record['tijdscore']}</td>
       <td>{$record['bericht']}</td>
       <td>{$record['tevredenheids score']}</td>
     </tr>";
   }
   
?>

<!doctype html>
<html lang="en">
  <head>

  <link rel="stylesheet" href="css\styling.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  </head>
  <body>
  <div class="container-fluid"> 
<h1> Resultaten</h1> 
</div>
        <table id="resultaat"> 
    <thead>
        <tr>
        <th>Volledige naam</th>
        <th>email</th>
        <th>tijdscore</th>
        <th>bericht</th>
        <th>tevredenheids score</th>
        </tr>
    </thead>
    <tbody>
        <?php
            echo $records;
        ?>
    </tbody>
    </table>
          
     
    
    <div class="sidenav" id="mySidenav">
    <a href="./index.html" id="home">Home</a>
    <a href="./Overons.html" id="overons">Over ons</a>
    <a href="./Contact.php" id="contact">Contact</a>
    <a href="./Score.php" id="score">Score</a>
    <a href="./Activiteit.html" id="activiteit">Activiteit</a>
    <a href="./resultaten.php" id="resultaten">Resultaten</a>
  </div>

  </main>

  </body>
</html>